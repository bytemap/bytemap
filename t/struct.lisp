(in-package #:bytemap-test)

(in-suite :bytemap)

(defbitstruct triple
  (c 0 :type (integer 46 42042022404040420204024040420420420420420))
  (a 0 :type (unsigned-byte 31))
  (b 0 :type (integer 0 1)))

(test bitstruct
  (5am:for-all ((len (5am:gen-integer :max 100 :min 1)))
    (let ((tv (make-triple-vector len)))
      (loop for i below len do
	    (is (= 0 (triple-a (triple-ref tv i))))
	    (is (= 0 (triple-b (triple-ref tv i))))
	    (is (= 46 (triple-c (triple-ref tv i))))

	    (incf (triple-a (triple-ref tv i)) (1- (expt 2 30)))
	    (incf (triple-b (triple-ref tv i)))
	    (incf (triple-c (triple-ref tv i)))

	    (is (= (triple-a (triple-ref tv i)) (1- (expt 2 30))))
	    (is (= 1 (triple-b (triple-ref tv i))))
	    (is (= 47 (triple-c (triple-ref tv i))))

	    (incf (triple-a (triple-ref tv i)) (expt 2 30))

	    (is (= (triple-a (triple-ref tv i)) (1- (expt 2 31))))))))

