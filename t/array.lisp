(in-package #:bytemap-test)

(in-suite :bytemap)

(test make-bytemap
  (5am:for-all ((len (5am:gen-integer :max 1000000 :min 1000)) (width (5am:gen-integer :max 1000 :min 1)))
    (let ((bm (make-bytemap len)))
      (loop for i from 0 to (floor len width) by width do
	    (setf (bytemap-int bm i width) (mod i (expt 2 width))))
      (loop for i from 0 to (floor len width) by width do
	    (5am:is (= (bytemap-int bm i width) (mod i (expt 2 width))))))))

(test ref
  (5am:for-all ((len (5am:gen-integer :max 10000 :min 1000)) (width (5am:gen-integer :max 1000 :min 1)))
    (let ((bm (make-bytemap len)))
      (loop for i from 0 to (floor len width) by width do
	    (let ((ref (bytemap-ref bm i)))
	      (setf (bytemap-int ref 0 width) (mod i (expt 2 width)))))
      (loop for i from 0 to (floor len width) by width do
	    (let ((ref (bytemap-ref bm i)))
	      (5am:is (= (bytemap-int ref 0 width) (bytemap-int bm i width) (mod i (expt 2 width)))))))))

