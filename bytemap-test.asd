(cl:defpackage #:bytemap-test.system
  (:use #:cl))
(cl:in-package #:bytemap-test.system)

(asdf:defsystem #:bytemap-test
  :name "bytemap-test"
  :author "John Fremlin <john@fremlin.org>"
  :version "prerelease"
  :description "Tests for bytemap"

  :components (
	       (:module :t
			:components (
				     (:file "suite")
				     (:file "array" :depends-on ("suite"))
				     (:file "struct" :depends-on ("suite")))))
  :depends-on (
	       :fiveam
	       :bytemap))
