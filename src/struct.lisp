(in-package #:bytemap)

(defstruct slot
  name
  default-value
  min
  max
  offset
  len
  count)

(defun slot-total-len (ps)
  (* (slot-len ps) (slot-count ps)))

(defun slot-type (s)
  `(integer ,(slot-min s) ,(slot-max s)))

(defun parse-slot-def (slot-def)
  (destructuring-bind (name default-value &key type (count 1))
      slot-def
    (multiple-value-bind (min max)
	(ecase (first type)
	  (integer
	   (destructuring-bind
		 (min max)
	       (rest type)
	     (values min max)))
	  (unsigned-byte
	   (destructuring-bind
		 (len)
	       (rest type)
	     (values 0 (1- (expt 2 len))))))
      (make-slot :name name :default-value default-value :min min :max max :count count :len (ceiling (log (- (1+ max) min) 2))))))

(defun generate-bitstruct-vector (name len)
  (let ((vector-type (concat-sym name '-vector)))
    `(progn
       (defun ,(concat-sym-package (symbol-package vector-type) 'make- vector-type) (len)
	 (make-bytemap (* ,len len)))
       (defun ,(concat-sym-package (symbol-package vector-type) 'mmap- vector-type) (file len &rest mmap-args)
	 (apply 'mmap-bytemap file (* ,len len) mmap-args))
       (defun-speedy ,(concat-sym name '-ref) (vector index)
	 (bytemap-ref vector (fast-bit-index-* ,len index))))))

(defun generate-defbitstruct (name slot-defs)
  (let ((slots (mapcar 'parse-slot-def slot-defs)))
    (flet ((def-func (name lambda-list input-type output-type &rest body)
	     `(progn
		(declaim (ftype (function ,input-type ,output-type) ,name))
		(defun-speedy ,name ,lambda-list
		  ,@body))))
      (let ((len 0))
	(loop for s in slots
	      do (setf (slot-offset s) len)
	      (incf len (slot-total-len s)))
	`(progn
	   (defmacro ,(concat-sym name '-update) (,name &rest name-values)
	     (with-unique-names (ref)
	       `(let ((,ref ,,name))
		  (declare (dynamic-extent ,ref))
		  ,@(loop for (slot-name value) on name-values by #'cddr
			  collect `(setf (,(concat-sym ',name '- slot-name) ,ref) ,value))
		  (values))))
	   ,@(loop for s in slots collect
		   (def-func (concat-sym name '- (slot-name s)) `(,name &optional (index 0))
		     `(bytemap &optional (integer 0 ,(1- (slot-count s))))
		     (slot-type s)
		     `(declare (dynamic-extent ,name))
		     `(+ (bytemap-int ,name (fast-bit-index-+ (fast-bit-index-* index ,(slot-len s)) ,(slot-offset s)) ,(slot-len s)) ,(slot-min s)))
		   collect
		   (def-func `(setf ,(concat-sym name '- (slot-name s))) `(new-value ,name &optional (index 0))
		     `(,(slot-type s) bytemap &optional (integer 0 ,(1- (slot-count s))))
		     (slot-type s)
		     `(declare (dynamic-extent ,name))
		     `(setf (bytemap-int ,name (fast-bit-index-+ (fast-bit-index-* index ,(slot-len s)) ,(slot-offset s)) ,(slot-len s)) (- new-value ,(slot-min s)))
		     `new-value))

	   ,(generate-bitstruct-vector name len)
	   ',name)))))


(defmacro defbitstruct (name &rest slot-defs)
  (generate-defbitstruct name slot-defs))
