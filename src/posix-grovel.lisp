(in-package #:bytemap)

(include "sys/stat.h" "fcntl.h" "sys/mman.h")

#.`(progn ,@(loop for c in '(O_RDWR O_WRONLY O_RDONLY 
			     O_CREAT
			     PROT_READ PROT_WRITE PROT_EXEC 
			     MAP_SHARED MAP_PRIVATE MAP_FAILED MAP_ANONYMOUS)
	  collect `(constant (,c ,(string-upcase (symbol-name c))))))

(ctype size_t "size_t")
(ctype off_t "off_t")
(ctype mode_t "mode_t")
