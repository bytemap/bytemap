(in-package #:bytemap)

(defun concat-sym (&rest args)
  (apply 'concat-sym-package (symbol-package (first args)) args))

(defun concat-sym-package (package &rest args)
  (intern (apply 'concatenate 'string (mapcar 'symbol-name args)) 
	  package))

(defmacro defun-speedy (name lambda-list &body body)
  `(progn
     (declaim (inline ,name)) 
     (defun ,name ,lambda-list
       (declare (optimize speed))
       ,@body)))
