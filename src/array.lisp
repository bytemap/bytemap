(in-package #:bytemap)

(defun-speedy array-bytemap-width (bytemap)
  (declare (ignore bytemap))	   
  +array-bytemap-width+)

(deftype bytemap-array (&optional (len '*))
  `(simple-array (unsigned-byte ,+array-bytemap-width+) ,len))

(define-bit-accessors bytemap-array)

(defun-speedy make-bytemap (bits)
  (declare (type bytemap-bit-index bits)) 
  (make-array (ceiling bits +array-bytemap-width+) :element-type '(unsigned-byte #.+array-bytemap-width+) :initial-element 0))
