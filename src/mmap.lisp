(in-package #:bytemap)

(defstruct mmap
  pointer
  (length-bytes 0 :type unsigned-byte))

(define-condition mmap-failed (error) 
  ((posix-error-number :initform errno :reader posix-error-number)
   (call-name :initarg :call))) 

(defun wrapped-munmap (address len)
  (let ((ret (posix-munmap address len)))
    (unless (zerop ret)
      (warn "posix-unmap(~A,~A) returned ~A" address len ret))))

(defun mmap-unmap (bm)
  (unless (zerop (mmap-length-bytes bm))
    (unwind-protect (wrapped-munmap (mmap-pointer bm) (mmap-length-bytes bm))
      (trivial-garbage:cancel-finalization bm)
      (setf (mmap-length-bytes bm) 0))))

(defun mmap (file &rest args &key length-bytes (shared t) (read t) (write t) (exec nil) 
	     (offset 0) (create-file t) anonymous
	     (create-mode #o666)
	     (ftruncate t))
  (assert (not (zerop length-bytes)))
  (typecase file
    (integer
     (let ((address
	    (posix-mmap (cffi:null-pointer) length-bytes 
			(logior (if read PROT_READ 0) (if write PROT_WRITE 0) (if exec PROT_EXEC 0))
			(logior (if shared MAP_SHARED MAP_PRIVATE) (if anonymous MAP_ANONYMOUS 0))
			file
			offset)))
       (when (cffi:pointer-eq address (cffi:inc-pointer (cffi:null-pointer) MAP_FAILED)) ; need to get a negative address sometimes
	 (error 'mmap-failed :call "mmap"))
       (let ((bm (make-mmap :pointer address :length-bytes length-bytes)))
	 (trivial-garbage:finalize bm (lambda()(wrapped-munmap address length-bytes)))
	 bm)))
    (string
     (let ((fd (posix-open file
			   (logior (if create-file O_CREAT 0)
				   (cond ((and read write) O_RDWR)
					 (read O_RDONLY)
					 (write O_WRONLY)
					 (t (error "Please specify read or write access for the mmap"))))
			   create-mode)))
       (when (= -1 fd)
	 (error 'mmap-failed :call "open"))
 
       (unwind-protect 
	    (progn
	      (when ftruncate
		(unless (zerop (posix-ftruncate fd length-bytes))
		  (error 'mmap-failed :call "ftruncate")))
	      (apply 'mmap fd args))
	 (let ((ret (posix-close fd)))
	   (unless (zerop ret)
	     (warn "posix-close on ~A failed" file))))))
    (null
     (apply 'mmap -1 :anonymous t args))))

(defun mmap-bytemap (file length-bits &rest mmap-args)
  (apply 'mmap file :length-bytes (ceiling length-bits 8) mmap-args))

(macrolet ((mmap-elt (mmap index)
  `(cffi:mem-aref (mmap-pointer ,mmap) ,+cffi-int-type+ ,index)))
  (define-bit-accessors mmap mmap-elt))

