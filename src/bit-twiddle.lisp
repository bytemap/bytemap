(in-package #:bytemap)

(deftype bytemap-elem-bit ()
  `(integer 0 #.(1- +array-bytemap-width+)))

(deftype bytemap-index ()
  `(integer 0 #.(ceiling most-positive-fixnum +array-bytemap-width+)))

(deftype positive-fixnum ()
  `(integer 0 #.most-positive-fixnum))

(deftype bytemap-bit-index ()
  'positive-fixnum)

(defmacro reversed-setf (a b) `(setf ,b ,a))

(defun generate-bit-accessor-maxlen (initial-value setf elt bytemap-type &optional (max-len '*))
  `(macrolet ((with-type-declarations (&body body)
		`(locally 
		     (declare 
		      (optimize speed (safety 0))
		      (type bytemap-index pos)
		      (type (unsigned-byte ,',max-len) val)
		      (type (integer 0 ,',(if (numberp max-len) (1- max-len) max-len)) bits-done)
		      (type bytemap-elem-bit bits-todo))
		   ,@body)))
     (locally
	 (declare (type ,bytemap-type bytemap))
       (let ((val ,initial-value))
	 (multiple-value-bind (start start-bit)
	     (locally 
		 (declare (optimize speed (safety 0))
			  (type bytemap-bit-index offset))
	       (floor offset +array-bytemap-width+))
	   (let ((bits-done 0) (pos start))
	     (declare (type (integer 0 ,max-len) len bits-done) (type bytemap-elem-bit start-bit))
	     (let ((bits-todo (min len (- +array-bytemap-width+ start-bit))))
	       (with-type-declarations
		   (,setf (ldb (byte bits-todo 0) val) (ldb (byte bits-todo start-bit) (,elt bytemap pos)))
		 (incf pos)
		 (incf bits-done bits-todo)))
	     ,(unless (and (numberp max-len) (<= max-len +array-bytemap-width+)) 
		      `(loop for bits-todo = (- len bits-done)
			     while (<= +array-bytemap-width+ bits-todo)
			     do
			     (with-type-declarations
				 (,setf (ldb (byte +array-bytemap-width+ bits-done) val) (,elt bytemap pos))
			       (incf pos)
			       (incf bits-done +array-bytemap-width+))))
	     (let ((bits-todo (- len bits-done)))
	       (with-type-declarations
		   (,setf (ldb (byte bits-todo bits-done) val) (ldb (byte bits-todo 0) (,elt bytemap pos))))
	       (with-type-declarations
		   val))))))))

(defun generate-bit-accessor (initial-value setf elt bytemap-type)
  (check-type initial-value (or symbol integer))
  (flet ((gen (len) 
	   (generate-bit-accessor-maxlen initial-value setf elt bytemap-type len)))
    `(locally
	 (declare (type positive-fixnum len))
       (cond
	 ,@(loop for max-len in '(#.(1- +fixnum-bits+))
		 collect `((<= len ,max-len)
			   ,(gen max-len)))
	 (t ,(gen '*))))))

(defmacro define-bit-accessors (type &optional (elt 'elt))
  `(progn
     (defun-speedy ,(concat-sym type '-bytemap-int) (bytemap offset len)
       (declare (dynamic-extent bytemap))
       ,(generate-bit-accessor 0 'setf elt type))
     (defun-speedy ,(concat-sym type '-bytemap-int-set) (bytemap offset len new-value)
       (declare (dynamic-extent bytemap))
       ,(generate-bit-accessor 'new-value 'reversed-setf elt type))))
