(in-package #:bytemap)

(cffi:defcfun (posix-mmap "mmap")
    :pointer
  (addr :pointer)
  (len size_t)
  (prot :int)
  (flags :int)
  (fildes :int)
  (off off_t))

(cffi:defcfun (posix-munmap "munmap")
    :int
  (addr :pointer)
  (len size_t))

(cffi:defcfun (posix-open "open")
    :int
  (path :string)
  (oflag :int)
  (mode mode_t))

(cffi:defcfun (posix-close "close")
    :int
  (fildes :int))

(declaim (inline %var-accessor-errno))
(cffi:defcvar ("errno" errno) :int)

(cffi:defcfun strerror :string
  (errno :int))

(defun posix-error-string ()
  (strerror errno))

(cffi:defcfun (posix-ftruncate "ftruncate")
    :int
  (fd :int)
  (length off_t))
