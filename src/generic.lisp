(in-package #:bytemap)

(macrolet ((def-binary-ops (&rest ops)
	     `(progn
		,@(loop for op in ops collect
			`(defun-speedy ,(concat-sym 'fast-bit-index- op) (a b)
			   (declare (optimize speed (safety 0))
				    (type bytemap-bit-index a b))
			   (the bytemap-bit-index (,op a b)))))))
  (def-binary-ops + - *))

(defmacro defreftype (type)
  `(progn
     (declaim (inline ,(concat-sym-package (symbol-package type) 'make- type '-ref)))     ; for SBCL's dynamic extent allocation
     (defstruct ,(concat-sym type '-ref)
       (bytemap nil :type ,type)
       (offset 0 :type bytemap-bit-index))
     (defun-speedy ,(concat-sym type '-bytemap-ref) (,type offset)
       (,(concat-sym 'make- type '-ref) :bytemap ,type :offset offset))
     (defun-speedy ,(concat-sym type '-ref-bytemap-ref) (ref offset)
       (,(concat-sym 'make- type '-ref) :bytemap (,(concat-sym type '-ref-bytemap) ref) :offset (fast-bit-index-+ (,(concat-sym type '-ref-offset) ref) offset)))

     (defun-speedy ,(concat-sym type '-ref-bytemap-int) (ref offset len)
       (declare (dynamic-extent ref))
       (,(concat-sym type '-bytemap-int) (,(concat-sym type '-ref-bytemap) ref) (fast-bit-index-+ offset (,(concat-sym type '-ref-offset) ref)) len))
     
     (defun-speedy ,(concat-sym type '-ref-bytemap-int-set) (ref offset len new-value)
       (declare (dynamic-extent ref))
       (,(concat-sym type '-bytemap-int-set) (,(concat-sym type '-ref-bytemap) ref) (fast-bit-index-+ offset (,(concat-sym type '-ref-offset) ref)) len new-value))
     ',(concat-sym type '-ref)))

#.(let ((types '(bytemap-array mmap)) (funcs '( (bytemap-int . (offset len)) 
					  ( bytemap-int-set . (offset len new-value))
					  ( bytemap-ref . (offset)))))
    (let ((all-types (loop for a in types collect a
			   collect (concat-sym a '-ref))))
      (flet ((generate-typecase-func (name args types)
	       `(defun-speedy ,name (bytemap ,@args)
		  (declare (dynamic-extent bytemap))
		  (etypecase bytemap
		    ,@(loop for a in types collect
			    `(,a (,(concat-sym a '- name) bytemap ,@args)))))))
	`(progn
	   ,@(loop for a in types
		   collect `(defreftype ,a))
	   (deftype bytemap () '(or ,@all-types))

	   ,@(loop for (name . args) in funcs
		   collect (generate-typecase-func name args all-types))))))

(defun-speedy (setf bytemap-int) (new-value bytemap offset len)
  (declare (dynamic-extent bytemap))
  (bytemap-int-set bytemap offset len new-value)
  new-value)
