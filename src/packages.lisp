(cl:defpackage #:bytemap
  (:use #:common-lisp)
  (:import-from #:cl-utilities #:with-unique-names #:once-only)
  (:export
   #:defbitstruct
   #:bytemap
   #:bytemap-array
   #:bytemap-mmap
   #:bytemap-int
   #:bytemap-ref
   #:make-bytemap
   #:mmap-bytemap
   #:mmap))

