(in-package #:bytemap)

(defconstant +fixnum-bits+ (integer-length (1+ (- most-positive-fixnum most-negative-fixnum))))
(defconstant +array-bytemap-width+ (expt 2 (floor (log +fixnum-bits+ 2)))) ; must be smaller than a fixnum on sbcl


(defconstant +cffi-int-type+ (intern (concatenate 'string (symbol-name :uint) (format nil "~A" +array-bytemap-width+)) :keyword))
