(cl:defpackage #:bytemap.system
  (:use #:cl))
(cl:in-package #:bytemap.system)

(cl:eval-when (:load-toplevel :execute)
  (asdf:operate 'asdf:load-op 'cffi-grovel))

(asdf:defsystem #:bytemap
  :name "bytemap"
  :author "John Fremlin <john@fremlin.org>"
  :version "prerelease"
  :description "Fast access to data stored in bitfields of normal or mmap'd arrays"

  :components (
	       (:module :src
			:components (
				     (cffi-grovel:grovel-file "posix-grovel" :depends-on ("packages"))
				     (:file "posix" :depends-on ("posix-grovel"))
				     (:file "mmap" :depends-on ("posix" "bit-twiddle"))
				     (:file "packages")
				     (:file "constants" :depends-on ("packages"))
				     (:file "utils" :depends-on ("packages"))
				     (:file "bit-twiddle" :depends-on ("constants" "utils"))
				     (:file "array" :depends-on ("bit-twiddle"))
				     (:file "generic" :depends-on ("array" "mmap"))
				     (:file "struct" :depends-on ("generic")))))
  :depends-on (
	       :cl-utilities
	       :cffi
	       :trivial-garbage))


